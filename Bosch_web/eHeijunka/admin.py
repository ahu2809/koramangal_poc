# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from .models import WebUser,Part, Prioritization, Acceptance, Mdata
from .forms import WebUserAdminForm


# Register your models here.
class WebUserAdmin(admin.ModelAdmin):
    form = WebUserAdminForm


class PartAdmin(admin.ModelAdmin):
    pass


class PrioritizationAdmin(admin.ModelAdmin):
    pass


class AcceptanceAdmin(admin.ModelAdmin):
    pass


class MdataAdmin(admin.ModelAdmin):
    pass


admin.site.register(WebUser, WebUserAdmin)
admin.site.register(Part, PartAdmin)
admin.site.register(Mdata, MdataAdmin)
admin.site.register(Acceptance, AcceptanceAdmin)
admin.site.register(Prioritization, PrioritizationAdmin)
