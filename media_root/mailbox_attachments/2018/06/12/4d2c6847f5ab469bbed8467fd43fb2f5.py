
from django.shortcuts import render, redirect
from clearpass_tool.role_mapping import get_rolenames, get_role_id_from_role_name_xml,huid_to_hkey
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from pyclearpass.clearpass_api import ClearpassApi
from pyclearpass.models import Role, Device
from pyclearpass.enums import AlertType
from pyclearpass.exceptions import ClearpassException,PersonAPIException,\
    UserNotFoundException,RoleNotFoundException, XMLParseException
import types
import logging
from django.conf import settings
from datetime import datetime
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from pyclearpass.clearpass_api import ClearpassApi
from pyclearpass.models import Role, Device
from pyclearpass.enums import AlertType
from django.contrib.auth import authenticate, login, logout
import types
import logging
from django.conf import settings
from datetime import datetime
from clearpass_tool.forms import UserLoginForm, DeviceForm

hostname = settings.CLEARPASS_TOOL_CLEARPASS_URL
client_id = settings.CLEARPASS_TOOL_CLEARPASS_USERNAME
client_secret = settings.CLEARPASS_TOOL_CLEARPASS_PASSWORD

api_object = ClearpassApi(fqdn=hostname, client_id=client_id, client_secret=client_secret)

LOGGER = logging.getLogger(__name__)

@require_http_methods(["GET", "POST"])
def loginuser(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            password = userObj['password']
            user = authenticate(username=username, password=password)
            if user is None:
                alert_message = "Please enter valid User Credentials"
                alert_type = "alert-danger"
                form = UserLoginForm()
                return render(request, 'clearpass_tool/login.html',
                              {'form': form, 'alertMessage': alert_message, 'alertType': alert_type})
            else:
                login(request, user)
                return redirect('landing')

        else:
            alert_message = "Please enter valid Username / password"
            alert_type = "alert-danger"
            form = UserLoginForm()
            return render(request, 'clearpass_tool/login.html',
                          {'form': form, 'alertMessage': alert_message, 'alertType': alert_type})

    else:
        form = UserLoginForm()
        return render(request, 'clearpass_tool/login.html', {'form': form})


@login_required()
@require_http_methods(["GET", "POST"])
def logoutuser(request):
    logout(request)
    return redirect('loginuser')


@login_required()
@require_http_methods(["GET", "POST"])
def landing(request):
    return render(request, "clearpass_tool/landing.html")


@login_required()
@require_http_methods(["GET", "POST"])
def proxyReg(request):
    role_names = get_rolenames(request.user.username)
    if request.method == "POST":
        try:
            mac = request.POST.get('mac_address', '')
            device_details = api_object.list_device_details_by_mac_address(Device(mac=mac))
            if device_details is None:

                role_name = request.POST.get('role_name', '')
                role_id = get_role_id_from_role_name_xml(role_name)
                notes = request.POST.get('notes', '')
                sponsor_name = request.POST.get('sponsor_name', '')
                get_hkey = huid_to_hkey(sponsor_name)
                api_device = Device(mac=mac, role_id=role_id, notes=notes, sponsor_name=get_hkey)
                response = api_object.proxy_reg_device(api_device)
                if type(response) is Device:
                    alert_message = "Device created successfully."
                    alert_type = AlertType.SUCCESS
                    LOGGER.info(alert_message)
            else:
                alert_message = "Device already exists."
                alert_type = AlertType.DANGER
                LOGGER.info(alert_message)

        except ClearpassException:
            alert_message = "Proxy registration Failed."
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)
        except (RoleNotFoundException,XMLParseException,UserNotFoundException,PersonAPIException) as e:
            alert_message = str(e)
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)

        
        return render(request, 'clearpass_tool/proxyReg.html',
                          {'alertMessage': alert_message, 'alertType': alert_type, 'roleNames': role_names})
    else:
        return render(request, 'clearpass_tool/proxyReg.html', {'roleNames': role_names})


@login_required()
@require_http_methods(["GET", "POST"])
def deleteDevice(request):
    role_names = get_rolenames(request.user.username)
    if request.method == "POST":
        try:
            mac = request.POST.get('mac_address', '')
            device_details = api_object.list_device_details_by_mac_address(Device(mac=mac))
            if device_details is not None and device_details.role_name in role_names:
                response = api_object.delete_device(device_details)
                alert_message = "Device deleted Successfully"
                alert_type = AlertType.SUCCESS
                LOGGER.info(alert_message)

            else:
                alert_message = "Permission denied to delete/Mac address not found"
                alert_type = AlertType.DANGER
                LOGGER.info(alert_message)

        except ClearpassException:
            alert_message = "Device Deletion Failed."
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)

        return render(request, 'clearpass_tool/deleteDevice.html',
                      {'alertMessage': alert_message, 'alertType': alert_type, 'roleNames': role_names})
    else:
        return render(request, 'clearpass_tool/deleteDevice.html', {'roleNames': role_names})

@login_required()
@require_http_methods(["GET", "POST"])
def deviceMac(request):
    if request.method == "POST":
        mac = request.POST.get('mac_address', '')
        api_device = Device(mac=mac)
        response = api_object.list_device_details_by_mac_address(api_device)
        if response is not None:
            alert_message = "Device Details Loaded Successfully for given MAC: {}.".format(mac)
            alert_type = AlertType.SUCCESS
            keys = ['mac', 'sponsor_name', 'current_state', 'expire_time', 'role_id', 'role_name']
            devicedata = response.__dict__
            devicedata['expire_time'] = datetime.fromtimestamp(devicedata['expire_time'])
            devicedata = [(k, v) for k, v in devicedata.items() if k in keys]
            LOGGER.info(alert_message)
            return render(request, 'clearpass_tool/deviceMac.html',
                                  {'alertMessage': alert_message, 'alertType': alert_type, 'info': devicedata})
        else:
            alert_message = "Device not found for given MAC: {}.".format(mac)
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)
            return render(request, 'clearpass_tool/deviceMac.html',
                      {'alertMessage': alert_message, 'alertType': alert_type})
    else:
        return render(request, 'clearpass_tool/deviceMac.html')


@login_required()
@require_http_methods(["GET", "POST"])
def devicesRole(request):
    role_names = get_rolenames(request.user.username)
    if request.method == "POST":
        try:
            role_name = request.POST.get('role_name', '')
            role_id = get_role_id_from_role_name_xml(role_name)
            api_device = Device(role_name=role_name, role_id=role_id)
            response = api_object.list_devices_by_role(api_device)
            keys = ['mac', 'sponsor_name', 'current_state', 'expire_time', 'role_id', 'role_name']
            values = []
            for device in response or []:
                device_dict = device.to_dict()
                device_dict['expire_time'] = datetime.fromtimestamp(device_dict['expire_time'])
                device_list = []
                for each_key in keys:
                    device_list.append(device_dict.get(each_key, ""))

                values.append(device_list)
            alert_message = "Devices retrieved based on Role Successfully."
            alert_type = AlertType.SUCCESS
            LOGGER.info(alert_message)
            return render(request, 'clearpass_tool/devicesRole.html',
                      {'alertMessage': alert_message, 'alertType': alert_type, 'keys': keys, 'values': values,
                           'roleNames': role_names})
            # else:
            #     alert_message = "Devices not Found."
            #     alert_type = AlertType.DANGER
            #     LOGGER.info(alert_message)
            #     return render(request, 'clearpass_tool/devicesRole.html',
            #           {'alertMessage': alert_message, 'alertType': alert_type, 'roleNames': role_names})
            #
        except ClearpassException:
            alert_message = "Retrieval of devices failed."
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)
            return render(request, 'clearpass_tool/devicesRole.html',
                          {'alertMessage': alert_message, 'alertType': alert_type, 'roleNames': role_names})

        except (RoleNotFoundException, XMLParseException) as e:
            alert_message = str(e)
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)
            return render(request, 'clearpass_tool/devicesRole.html',
                      {'alertMessage': alert_message, 'alertType': alert_type, 'roleNames': role_names})
    else:
        return render(request, 'clearpass_tool/devicesRole.html', {'roleNames': role_names})

    
@login_required()
@require_http_methods(["GET", "POST"])
def roleManagement(request):
    role_names = get_rolenames(request.user.username)
    if request.method == "POST":
        try:
            mac = request.POST.get('mac_address', '')
            device_details = api_object.list_device_details_by_mac_address(Device(mac=mac))
            if device_details is not None:
                role_name = request.POST.get('role_name', '')
                role_id = get_role_id_from_role_name_xml(role_name)
                sponsor_name = device_details.sponsor_name
                api_device = Device(mac=mac, role_id=role_id, sponsor_name=sponsor_name)
                response = api_object.update_role_for_mac(api_device)
                if type(response) is Device:
                    alert_message = "Device role updated Successfully"
                    alert_type = AlertType.SUCCESS
                    LOGGER.info(alert_message)
            else:
                alert_message = "Device not found."
                alert_type = AlertType.DANGER
                LOGGER.error(alert_message)

        except ClearpassException:
            alert_message = "Device update failed."
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)
        except (RoleNotFoundException, XMLParseException) as e:
            alert_message = str(e)
            alert_type = AlertType.DANGER
            LOGGER.error(alert_message)
        
        return render(request, 'clearpass_tool/roleManagement.html',
                          {'alertMessage': alert_message, 'alertType': alert_type, 'roleNames': role_names})

    else:
        return render(request, 'clearpass_tool/roleManagement.html', {'roleNames': role_names})
