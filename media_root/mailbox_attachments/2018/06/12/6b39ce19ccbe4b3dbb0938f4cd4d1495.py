from typing import Dict, List, Callable, TypeVar, Union
from netaddr import EUI
from datetime import datetime, timedelta

Model = TypeVar('Model', covariant=True)
DEFAULT_CPPM_SERVER_DATE = datetime(1969, 12, 31, 17, 00, 00).isoformat()
server_date = datetime.strptime(DEFAULT_CPPM_SERVER_DATE, '%Y-%m-%dT%H:%M:%S')
DEFAULT_EXPIRATION_DAYS = 180
current_datetime = datetime.strptime(datetime.now().replace(microsecond=0).strftime('%Y-%m-%dT%H:%M:%S'), '%Y-%m-%dT%H:%M:%S')

def _make(model: Callable[..., Model], data: List[Dict]) -> List[Model]:
    """
    Converts raw data into modeled objects
    :param model: the data model to use
    :param data: the data, as a list of dictionaries
    :return: a list of models
    """
    return [model(**d) for d in data]


class Response:
    """
    Defines a generic API response object
    """

    def __init__(self, success: bool, status_code: int, message: str, data: Dict = None):
        """
        Constructs Response objects
        :param success: Whether or not the response was returned successfully or not
        :param status_code: The status code of the response
        :param message: The contents of the response message in the case of an error
        :param data: A dictionary containing the raw response values returned
        """

        self.success = success
        self.status_code = status_code
        self.data = data
        self.message = message


class Device:
    """
        Defines a device object

    """
    @staticmethod
    def date_conversion(current_time) -> int:
        """
        Performs date calculations and returns end time in seconds
        :return: time in seconds
        """

        diff = (current_time - server_date).total_seconds()
        end_time = diff + (timedelta(days=DEFAULT_EXPIRATION_DAYS)).total_seconds()
        return int(end_time)

    def __init__(self, mac: EUI=None, role_id: int = None, notes: str="", sponsor_name: str="", id: str = None, username: str=None,
                 start_time: int = 0, expire_time: int =0,
                 sponsor_profile: str="", enabled: bool=None, current_state: str="", visitor_name: str=None,
                 harvard_source: str="", airgroup_enable: str="", airgroup_shared: str="",
                 visitor_carrier: str=None, role_name: str="", mac_auth: bool=None, source: str="",
                 create_time: int=0, sponsor_profile_name: str="",
                 _links: Dict[str, Dict[str, str]]=None, **kwargs):

        self.id = id
        self.username = username
        self.sponsor_name = sponsor_name
        self.sponsor_profile = sponsor_profile
        self.enabled = enabled
        self.current_state = current_state
        self.notes = notes
        self.visitor_name = visitor_name
        self.harvard_source = harvard_source
        self.airgroup_enable = airgroup_enable
        self.airgroup_shared = airgroup_shared
        self.visitor_carrier = visitor_carrier
        self.role_name = role_name
        self.role_id = role_id
        self.mac = mac
        self.mac_auth = mac_auth
        self.source = source
        self.create_time = create_time
        if create_time == 0:
            # self.create_time = int(datetime.now().replace(microsecond=0).timestamp())
            self.create_time = int((current_datetime - server_date).total_seconds())
        self.start_time = start_time
        if start_time == 0:
            self.start_time = self.create_time
        self.expire_time = expire_time
        if expire_time == 0:
            self.expire_time = self.date_conversion(current_datetime)
        self.sponsor_profile_name = sponsor_profile_name
        self._links = _links
        self.__dict__.update(kwargs)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def to_dict(self) -> Dict:
        """
        Returns a dictionary representation of the device that is able to be parsed into JSON for API requests
        :return: The dictionary containing the data for an API request
        """

        data = {"id": self.id,
                "username": self.username,
                "sponsor_name": self.sponsor_name,
                "sponsor_profile": self.sponsor_profile,
                "enabled": True,
                "current_state": self.current_state,
                "notes": self.notes,
                "visitor_name": "X-CLI-import-{}".format(self.mac),
                "harvard_source": "cli-import",
                "airgroup_enable": "",
                "airgroup_shared": "",
                "visitor_carrier": "",
                "role_name": self.role_name,
                "role_id": self.role_id,
                "mac": self.mac,
                "mac_auth": self.mac_auth,
                "source": self.source,
                "create_time": self.create_time,
                "start_time": self.start_time,
                "expire_time": self.expire_time,
                "sponsor_profile_name": self.sponsor_profile_name,
                "_links": self._links
                }
        return data

    def mac_conversion(self)->str:
        """
        Returns a string representation of the mac address.
        :return: a string
        """
        mac_address = str(self.mac).replace(":", "")
        return mac_address


class Role:
    """
        Defines a device object
    """
    def __init__(self, role_name: str="", role_id: int=None):

        self.role_id = role_id
        self.role_name = role_name
