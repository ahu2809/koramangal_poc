import json


def get_rolenames(user_eppns: str):

    with open('role_mapping.json','r') as f:
        role_names = []

        json_read = json.loads(f.read())
        for d in json_read['role_mappings']:
            for k, v in d.items():
                if user_eppns in v:
                    role_names.append(k)
            return role_names

# print(get_rolenames("7a7d04e2709036fe@harvard.edu"))



