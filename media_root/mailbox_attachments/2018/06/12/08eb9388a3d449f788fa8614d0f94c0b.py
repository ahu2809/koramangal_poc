import logging
from pyclearpass.rest_adapter import RestAdapter
from pyclearpass.models import Device, Model, Role,Response
from typing import Union, Iterator
import json
from datetime import datetime


class ClearpassApi:

    VERSION = "v6.5"
    today_date = datetime.strptime(datetime.now().replace(microsecond=0).isoformat(), '%Y-%m-%dT%H:%M:%S')

    def __init__(self, fqdn: str, client_id: str, client_secret: str,
                 limit: int = 1000, verify: bool = False, logger: logging.Logger = None):

        """
        Creates a Clearpass Api object used for interacting with the Aruba Clearpass.
        :param fqdn: the fully qualified domain name to connect to (protocol omitted)
        :param client_id: the public identifier to authenticate in oauth
        :param client_secret: the private identifier to authenticate in oauth
        :param limit: to maximum number of objects returned per page request
        :param verify: path to ssl certificates for verification (Default: False)
        :param logger: logger object to log to (Optional)
        """
        self._adapter = RestAdapter(fqdn, client_id, client_secret, ClearpassApi.VERSION, verify=verify)
        self._limit = limit
        self._logger = logger or logging.getLogger(__name__)
        # # below changes for xml api only
        # self._xml_auth = (username,password)
        # self.fqdn = fqdn

    def __enter__(self):
        self._adapter.start_session()
        return self

    def __exit__(self, *args):
        self._adapter.stop_session()

    def update_expiration(self, device: Device)->Device:
        """
        Updates expiration time for devices
        :param device: device object for Device class
        :return: updated device details
        """

        device.expire_time = Device.date_conversion(current_time=ClearpassApi.today_date)
        endpoint = "/device/mac/{}".format(device.mac)
        params = {"expire_time": device.expire_time, "create_time": device.create_time}
        response = self._adapter.patch(endpoint, json=device.to_dict(), params=params)
        return Device(**response)

    def list_devices(self, filter_key: str = None, filter_value: Union[int, str] = None)->Iterator[Model]:
        """
        Lists all devices/device_details with the provided data
        :param filter_key: The key to get device details or devices
        :param filter_value: The value of the respective key to get device details or devices
        :return: device objects
        """
        if filter_key is None or filter_value is None:
            params = {}
        else:
            params = {"filter": json.dumps({filter_key: filter_value})}

        endpoint = "/device"
        response = self._adapter.get(endpoint=endpoint, params=params)
        list_devices = response['_embedded']['items']
        for d in list_devices:
            yield Device(**d)

    def list_devices_by_key(self, sponsor: str)->Iterator[Model]:
        """
        Lists all devices by Harvard key
        :param sponsor: A sponsor to get list of devices
        :return: list of devices
        """
        return self.list_devices("sponsor_name", sponsor)

    def list_device_details_by_mac_address(self, device: Device)->Iterator[Model]:
        """
        Lists all device(s) details by given mac address
        :param device: device object for Device class
        :return: list of device(s) details
        """
        return next(self.list_devices("mac", device.mac), None)

    def list_device_details_by_device_id(self, device: Device)->Union[int, str]:
        """
        Lists all device(s) details by given device id
        :param device: device object for Device class
        :return: list of device(s) details
        """
        return next(self.list_devices("id", device.id), None)

    def create_device(self, device: Device)->Device:
        """
        Creates a device with the provided data
        :param device: device object for Device class
        :return: The newly-created Device
        """

        endpoint = "/device"
        response = self._adapter.post(endpoint, json=device.to_dict(), params={})
        return Device(**response)

    def delete_device(self, device: Device):
        """
        Deletes a device with the provided data
        :param device: device object for Device class
        :return: a boolean value
        """
        endpoint = "/device/mac/{}?change_of_authorization=true".format(device.mac)
        response = self._adapter.delete(endpoint)

    def delete_device_from_endpoints(self, device: Device):
        """
        Deletes a device with the provided data from the endpoints in CPPM
        :param device: device object for Device class
        :return: an boolean value
        """
        endpoint = "/endpoint/mac-address/{}".format(device.mac_conversion())
        response = self._adapter.delete(endpoint)

    def proxy_reg_device(self, device: Device)->Device:
        """
        Creates a device with the provided data on behalf of a proxy
        :param device: device object for Device class
        :return: The newly-created Device
        """

        endpoint = "/device"
        response = self._adapter.post(endpoint, json=device.to_dict(), params=None)
        return Device(**response)

    def update_role_for_mac(self, device: Device)->Device:
        """
        Updates the role_id,role_name for a device
        :param device: device object for Device class
        :return: The updated Device
        """

        endpoint = "/device/mac/{}".format(device.mac)
        response = self._adapter.patch(endpoint, json=device.to_dict(), params=None)
        return Device(**response)

    def get_role_by_name(self, role: Role)->Response:
        """
        Updates the role_id by role_name
        :param role: device object for Device class
        :return: role_id
        """
        endpoint = "/role/name/{}".format(role.role_name)
        response = self._adapter.get(endpoint)
        return response.get('id', None)

    def get_role_by_id(self, role: Role)->Response:
        """
        Updates the role_name by role_id
        :param role: device object for Device class
        :return: role_name
        """
        endpoint = "/role/{}".format(role.role_id)
        response = self._adapter.get(endpoint)
        return response.get('name', None)

    def list_devices_by_role(self, device: Device)->Iterator[Model]:
        """
        Lists all device(s) details by given role name
        :param device: device object for Device class
        :return: devices
        """
        return self.list_devices("role_name", device.role_name)

    def get_role_id_from_role_name_devices(self, device: Device):
        """
        Get role_id by given role name in devices
        :param device: device object for Device class
        :return: role_id
        """
        gen_obj = next(self.list_devices("role_name", device.role_name), None)
        dicts = gen_obj.__dict__
        return dicts['role_id']
