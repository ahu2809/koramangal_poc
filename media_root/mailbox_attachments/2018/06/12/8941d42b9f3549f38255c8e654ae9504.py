from unittest import TestCase
from unittest.mock import patch
from pyclearpass.pyclearpass.exceptions import ClearpassException
from pyclearpass.pyclearpass.clearpass_api import ClearpassApi
from pyclearpass.pyclearpass.models import Model,Response,Device
from pyclearpass.pyclearpass.enums import HttpMethod
from requests.status_codes import codes
from netaddr import EUI
from types import GeneratorType


class TestClearpassApi(TestCase):

    def setUp(self):
        self.test_id = 12346
        self.test_username = "Device2"
        self.test_start_time = "4/30/2018"
        self.test_expire_time = "4/30/2019"
        self.test_sponsor_name = "sponsor1"
        self.test_sponsor_profile = "sponsorprofile1"
        self.test_enabled = True
        self.test_current_state = False
        self.test_notes = "Sample notes"
        self.test_harvard_source = "cli-import"
        self.test_airgroup_enable = False
        self.test_airgroup_shared = False
        self.test_visitor_carrier = ""
        self.test_role_name = "Test role"
        self.test_role_id = 2
        self.test_mac = EUI('A1-bb-cc-dd-ee-ff')
        self.test_visitor_name = "X-CLI-import-{}".format(self.test_mac)
        self.test_mac_auth = True
        self.test_source = "test"
        self.test_create_time = "Test CT"
        self.test_sponsor_profile_name = "Test sponsor"
        self.test_links = ["Test"]

        self.test_device_dict = {
                                 "id": self.test_id,
                                 "username": self.test_username,
                                 "start_time": self.test_start_time,
                                 "expire_time": self.test_expire_time,
                                 "sponsor_name": self.test_sponsor_name,
                                 "sponsor_profile": self.test_sponsor_profile,
                                 "enabled": self.test_enabled,
                                 "current_state": self.test_current_state,
                                 "notes": self.test_notes,
                                 "visitor_name": self.test_visitor_name,
                                 "harvard_source": self.test_harvard_source,
                                 "airgroup_enable": self.test_airgroup_enable,
                                 "airgroup_shared": self.test_airgroup_shared,
                                 "visitor_carrier": self.test_visitor_carrier,
                                 "role_name": self.test_role_name,
                                 "role_id": self.test_role_id,
                                 "mac": self.test_mac,
                                 "mac_auth": self.test_mac_auth,
                                 "source": self.test_source,
                                 "create_time": self.test_create_time,
                                 "sponsor_profile_name": self.test_sponsor_profile_name,
                                 "_links": self.test_links
        }
        self.test_device = Device(self.test_notes,self.test_mac,self.test_role_id,self.test_sponsor_name,self.test_id, self.test_username,
                                  self.test_start_time, self.test_expire_time,self.test_sponsor_profile,
                                  self.test_enabled,self.test_current_state,self.test_visitor_name,self.test_harvard_source,
                                  self.test_airgroup_enable,self.test_airgroup_shared,self.test_visitor_carrier,
                                  self.test_role_name, self.test_mac_auth,self.test_source,self.test_create_time,
                                  self.test_sponsor_profile_name, self.test_links)

        self.test_device_mac = Device(self.test_notes,self.test_mac,self.test_role_id)

        # self.test_device_dict_cr = {"mac": self.test_mac, "role_id": self.test_role_id, "notes": self.test_notes}
        # self.test_device_dict_pr = {"mac": self.test_mac, "role_id": self.test_role_id, "notes": self.test_notes,
        #                             "sponsor_name": self.test_sponsor_name}
        #
        # self.test_device_cr = Device(self.test_notes, self.test_mac, self.test_role_id)
        # self.test_device_pr = Device(self.test_notes, self.test_mac, self.test_role_id, self.test_sponsor_name)

        self.good_test_device_result = [self.test_device_mac]
        self.test_device_result = {'result': [self.test_device_dict]}
        self.fqdn = "test.url"
        self.client_id = "test"
        self.client_secret = "test"
        self.clearpassapi = ClearpassApi(self.fqdn, self.client_id, self.client_secret)

        self.good_device_data = {'mac': '11:22:33:44:55:66', 'role_id': 2, 'notes': "test"}
        self.good_device = Device(**self.good_device_data)

        self.empty_response = Response(True, codes.ok, "test info log message", data={"result": []})
        self.bad_response = Response(False, codes.forbidden, "test warning log message")



    def test_list_devices(self):
        pass
        # with patch('pyclearpass.pyclearpass.rest_adapter.RestAdapter.get', return_value=self.test_device_result)as request:
        #     params = {'test': 'valid'}
        #     device = self.clearpassapi.list_devices(params)
        #     self.assertIs(type(device), GeneratorType)


            # self.assertEqual(device, self.test_device)
            # for item in device:
            #     print(item)


            # request.assert_called_with(params)
            # print(type(device),type(self.test_device))
            # self.assertEqual(device, self.test_device)

    #                 with patch('pyairwave3.wireless_api.WirelessApi._get', return_value=self.good_client_data):
    #             clients = self.wireless_api.get_clients_by({'test': 'valid'})
    #             self.assertIs(type(clients), GeneratorType)
    #             clients = list(clients)
    #             self.assertEqual(len(clients), len(self.good_clients))
    #             self.assertEqual(clients[0].__dict__, self.good_client.__dict__)


    def test_list_device_details_by_mac(self):
        pass
    #     with patch("pyclearpass.pyclearpass.clearpass_api.ClearpassApi.list_devices", return_value=self.good_test_device_result):
    #         # validate that a single device is extracted from the iterator and returned
    #         device = self.clearpassapi.list_device_details_by_mac_address(self.test_device_mac)
    #
    #         self.assertEqual(device.__dict__, self.good_test_device_result.__dict__)

    def test_list_device_details_by_id(self):
        pass

    def test_create_device(self):
        with patch('pyclearpass.pyclearpass.rest_adapter.RestAdapter.post', return_value=self.test_device_dict) as request:
            device = self.clearpassapi.create_device(self.test_device)
            request.assert_called_with('/device', json=self.test_device_dict, params={})
            self.assertEqual(device, self.test_device)
            print(device.__dict__)

    def test_proxy_reg_device(self):
        with patch('pyclearpass.pyclearpass.rest_adapter.RestAdapter.post', return_value=self.test_device_dict) as request:
            device = self.clearpassapi.create_device(self.test_device)
            request.assert_called_with('/device', json=self.test_device_dict, params={})
            self.assertEqual(device, self.test_device)

    def test_delete_device(self):
        pass
    #     with patch('pyclearpass.pyclearpass.rest_adapter.RestAdapter.delete', return_value=self.test_device_dict) as request:
    #         params = {'mac' : '11-22-33-44-55-66'}
    #         device = self.clearpassapi.delete_device(self.test_device)
    #         request.assert_called_with('/device', params={})
    #         self.assertEqual(device, self.test_device)


    def test_delete_devices_by_endpoint(self):
        pass




