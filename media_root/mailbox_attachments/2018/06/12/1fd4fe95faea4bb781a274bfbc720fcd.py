import logging
from pyclearpass.pyclearpass.rest_adapter import RestAdapter
from pyclearpass.pyclearpass.models import Device,Model
from typing import Union, Iterator
import json



class ClearpassApi:

    VERSION = "v6.5"

    # module_logger = logging.getLogger('clearpass_api')

    def __init__(self, fqdn: str, client_id: str, client_secret: str, limit: int = 1000, verify: bool = False,
                 logger: logging.Logger = None):

        """
        Creates a Clearpass Api object used for interacting with the Aruba Clearpass.
        :param fqdn: the fully qualified domain name to connect to (protocol omitted)
        :param client_id: the public identifier to authenticate in oauth
        :param client_secret: the private identifier to authenticate in oauth
        :param limit: to maximum number of objects returned per page request
        :param verify: path to ssl certificates for verification (Default: False)
        :param logger: logger object to log to (Optional)
        """
        self._adapter = RestAdapter(fqdn, client_id, client_secret, ClearpassApi.VERSION, verify=verify)
        self._limit = limit
        self._logger = logger or logging.getLogger('clearpass_app')
    def __enter__(self):
        self._adapter.start_session()
        return self

    def __exit__(self, *args):
        self._adapter.stop_session()

    def list_devices(self, key: str = None, value: Union[int, str] = None)->Iterator[Model]:
        """
        Lists all devices/device_details with the provided data
        :param key: The key to get device details or devices
        :param value: The value of the respective key to get device details or devices
        :return: device objects
        """
        if key is None or value is None:
            params = {}
        else:
            params = {"filter": json.dumps({key: value})}

        endpoint = "/device"
        response = self._adapter.get(endpoint=endpoint, params=params)
        list_devices = response['_embedded']['items']
        logging.INFO(type(list_devices))
        for d in list_devices:
            yield Device(**d)

    def list_devices_by_key(self, sponsor: str)->Iterator[Model]:
        """
        Lists all devices by Harvard key
        :param sponsor: A sponsor to get list of devices
        :return: list of devices
        """
        return self.list_devices("sponsor_name", sponsor)

    def list_device_details_by_mac_address(self, device: Device)->Iterator[Model]:
        """
        Lists all device(s) details by given mac address
        :param device: device object for Device class
        :return: list of device(s) details
        """
        return next(self.list_devices("mac", device.mac), None)

    def list_device_details_by_device_id(self, device: Device)->Union[int, str]:
        """
        Lists all device(s) details by given device id
        :param device: device object for Device class
        :return: list of device(s) details
        """
        return next(self.list_devices("id", device.id), None)

    def create_device(self, device: Device)->Device:
        """
        Creates a device with the provided data
        :param device: device object for Device class
        :return: The newly-created Device
        """
        endpoint = "/device"
        response = self._adapter.post(endpoint, json=device.to_dict(), params={})
        return Device(**response)

    def delete_device(self, device: Device):
        """
        Deletes a device with the provided data
        :param device: device object for Device class
        :return: a boolean value
        """
        endpoint = "/device/mac/{}?change_of_authorization=true".format(device.mac)
        response = self._adapter.delete(endpoint)

    def delete_device_from_endpoints(self, device: Device):
        """
        Deletes a device with the provided data from the endpoints in CPPM
        :param device: device object for Device class
        :return: an boolean value
        """
        endpoint = "/endpoint/mac-address/{}".format(device.mac_conversion())
        response = self._adapter.delete(endpoint)

    def proxy_reg_device(self, device: Device)->Device:
        """
        Creates a device with the provided data on behalf of a proxy
        :param device: device object for Device class
        :return: The newly-created Device
        """
        endpoint = "/device"
        response = self._adapter.post(endpoint, json=device.to_dict(), params={})
        return Device(**response)






