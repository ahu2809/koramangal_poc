from django.core.mail import EmailMultiAlternatives, send_mail
from django.template.loader import get_template
from django.template import Context, loader
from email.mime.image import MIMEImage
from django.conf import settings


def email_send(subject, html, context):
    # html_content = get_template(html).render(context)
    html_content = loader.render_to_string(html, context)
    from_email = "palamanibhaskar@gmail.com"
    to = "palamanibhaskar@gmail.com"
    # send_mail(subject,"PFA",from_email,[to],html_message=html_content)
    msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
    msg.content_subtype = "text/html"
    msg.attach_alternative(html_content, "text/html")

    images = ['Bosch_trans_logo.png', 'line.png']

    for img in images:
        fp = open(settings.STATICFILES_DIRS[0] + "\\img\\"+img, 'rb')
        msgImage = MIMEImage(fp.read(), 'png')
        fp.close()
        msgImage.add_header('Content-ID', '<' + img + '>')
        msgImage.add_header("Content-Disposition", "inline", filename=img)
        msg.attach(msgImage)
    msg.send()


import smtplib
import time
import imaplib
import email


ORG_EMAIL   = "palamanibhaskar@gmail.com"
FROM_EMAIL  = "palamanibhaskar@gmail.com"
FROM_PWD    = "Gmail_123"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT   = 993

# -------------------------------------------------
#
# Utility to read email from Gmail Using Python
#
# ------------------------------------------------

def read_email_from_gmail():
    try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FROM_EMAIL,FROM_PWD)
        mail.select('inbox')

        type, data = mail.search(None, 'ALL')
        mail_ids = data[0]


        id_list = mail_ids.split()
        first_email_id = int(id_list[0])
        latest_email_id = int(id_list[-1])
        print(first_email_id, latest_email_id)

        for i in range(latest_email_id, first_email_id, -1):

            typ, data = mail.fetch(str(i), "(RFC822)")

            for response_part in data[0]:
                if isinstance(response_part, tuple):
                    print("hio")
                    msg = email.message_from_string(response_part[1])
                    email_subject = msg['subject']
                    email_from = msg['from']
                    print('From : ' , email_from , '\n')
                    print('Subject : ' , email_subject , '\n')

    except Exception as e:
        print(str(e))



