# Generated by Django 2.0.5 on 2018-06-13 09:10

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('eHeijunka', '0004_acceptance_entrytimestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='acceptance',
            name='actual_completion',
            field=models.DateTimeField(default=datetime.datetime(2018, 6, 13, 9, 9, 56, 526940, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='acceptance',
            name='planned_completion',
            field=models.DateTimeField(default=datetime.datetime(2018, 6, 13, 9, 10, 3, 931460, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
