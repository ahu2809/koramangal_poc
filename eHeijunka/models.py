from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_mailbox.models import Message


class WebUser(models.Model):
    ROLES = (
        ('ppx', 'ppx'),
        ('pqa', 'pqa'),
        ('pmq', 'pmq'),
        ('adm', 'adm'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.CharField(max_length=3, blank=False, choices=ROLES, default='ppx')


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        WebUser.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.webuser.save()


# class WebUser(models.Model):
#
#     username = models.CharField(max_length=100, blank=False, primary_key=True)
#     password = models.CharField(max_length=50, blank=False)
#
#
#     # def __unicode__(self):
#     #     return self.username
#
#     def __repr__(self):
#         return self.username


class Part(models.Model):
    part_number = models.CharField(max_length=100, blank=False, primary_key=True )
    inspection_lead_time = models.IntegerField(default=0)

    def __repr__(self):
        return self.part_number


class Mdata(models.Model):
    # class Meta:
    #     unique_together = (('part_family', 'grninputpart'),)

    part_family = models.CharField(max_length=40, blank=False)
    grninputpart = models.CharField(max_length=40, blank=False)
    grninputdate = models.DateTimeField(auto_now_add=True, blank=True, editable=False, )

    def __repr__(self):
        return self.part_family+self.grninputpart


class Prioritization(models.Model):

    entrytimestamp = models.DateTimeField()
    part_family = models.CharField(max_length=40, blank=False, null= False)
    grninputpart = models.CharField(max_length=40, blank=False, null= False)
    # grninputdate = models.DateTimeField(editable=False)
    aging = models.CharField(max_length=40, blank=False, null= False)
    priority = models.CharField(max_length=10)
    remarks = models.CharField(max_length=40, blank=True, null=False)

    def __repr__(self):
        return self.part_family+self.grninputpart

class AdminApproval(models.Model):

    entrytimestamp = models.DateTimeField()
    part_family = models.CharField(max_length=40, blank=False, null= False)
    grninputpart = models.CharField(max_length=40, blank=False, null= False)
    # grninputdate = models.DateTimeField(editable=False)
    aging = models.CharField(max_length=40, blank=False, null= False)
    priority = models.CharField(max_length=10)
    remarks = models.CharField(max_length=40, blank=True, null=False)

    def __repr__(self):
        return self.part_family+self.grninputpart


class Acceptance(models.Model):
    entrytimestamp = models.DateTimeField()
    part_family = models.CharField(max_length=40, blank=False, null= False)
    grninputpart = models.CharField(max_length=40, blank=False)
    aging = models.CharField(max_length=40, blank=False, null= False)
    priority = models.CharField(max_length=10)
    remarks = models.CharField(max_length=40, blank=True, null=False)
    selection_to_test = models.BooleanField
    planned_completion = models.DateTimeField()
    actual_completion = models.DateTimeField()

    def __repr__(self):
        return self.part_family+self.grninputpart





