from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.loginuser, name='loginuser'),
    re_path(r'^loginuser$', views.loginuser, name='loginuser'),
    re_path(r'^logoutuser$', views.logoutuser, name='logoutuser'),

    re_path(r'^landing$', views.landing, name='landing'),
    re_path(r'^emailformat$', views.emailformat, name='emailformat'),


    re_path(r'^inputsheet$', views.inputsheet, name='inputsheet'),
    re_path(r'^post_inputsheet$', views.post_inputsheet, name='post_inputsheet'),
    re_path(r'^adminapproval$', views.adminapproval, name='adminapproval'),

    re_path(r'^prioritization$', views.prioritization, name='prioritization'),
    re_path(r'^post_prioritization', views.post_prioritization, name='post_prioritization'),

    re_path(r'^acceptance$', views.acceptance, name='acceptance'),
    re_path(r'^post_acceptance$', views.post_acceptance, name='post_acceptance'),

    re_path(r'^dashboard$', views.dashboard, name='dashboard'),
    re_path(r'^dashboard_edit$', views.dashboard_edit, name='dashboard_edit'),

    re_path(r'^reports$', views.reports, name='reports'),
    re_path(r'^nok$', views.nok, name='nok'),


    # path('proxyreg/', views.proxyReg, name='proxyreg'),

]