from django.shortcuts import render, redirect
from .forms import WebUserLoginForm
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from .models import Part, Mdata, Prioritization, WebUser, AdminApproval, Acceptance
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.utils import timezone
from datetime import datetime
import math
import pytz
from .lock_transac import LockedAtomicTransaction
from eHeijunka.utils import email_send


def get_role(user):
    return WebUser.objects.get(user=user).role


@require_http_methods(["GET", "POST"])
def loginuser(request):
    if request.method == 'POST':
        form = WebUserLoginForm(request.POST)
        if form.is_valid():
            print("FORM VAILD")
            # data = request.POST
            user_obj = form.cleaned_data
            username = user_obj['username']
            password = user_obj['password']
            user = authenticate(username=username, password=password)
            print(user)
            if user is None:
                alert_message = "Please enter valid user credentials"
                alert_type = "alert-danger"
                form = WebUserLoginForm()
                return render(request, 'registration/login.html', {'form': form, 'alertMessage': alert_message, 'alertType': alert_type})
            else:
                print("loggin in")
                login(request, user)
                # return HttpResponseRedirect('/thanks/')
                # return redirect('inputsheet')
                # data = Part.objects.all()
                # print(data)

                # return render(request, 'prioritization.html', {'data': data})
                # return redirect('prioritization')
                return redirect('landing')
        else:
            alert_message = "Invalid Form or Server Internal Error"
            alert_type = "alert-danger"
            form = form
            return render(request, 'registration/login.html', {'form': form, 'alertMessage': alert_message, 'alertType': alert_type})
    else:
        form = WebUserLoginForm()
        return render(request, 'registration/login.html', {'form': form})


@login_required()
@require_http_methods(["GET", "POST"])
def inputsheet(request):
    if get_role(request.user) != "adm":
        return redirect("landing")
    # if request.method == 'POST':
    #     inputsheet_data = request.get_json()
    #     primary_check = []
    #     for each in inputsheet_data:
    #         primary_check.append(each[0])
    #     if len(set(primary_check)) == len(primary_check):
    #         Part.objects.all().delete()
    #         for each in inputsheet_data:
    #             try:
    #                 Part.objects.create(part_number=each[0], inspection_lead_time=each[1])
    #             except Exception as ex:
    #                 print(each[0], ex)
    #         print("Information saved to DB")
    #         return JsonResponse(data="Information saved to DB")
    #         # return 'Data saved on Testing Raja DB'
    #     else:
    #         return JsonResponse(data="Duplicate Part Numbers")
    # else:
    data = Part.objects.all()
    print(len(data), " no of inputsheet values")
    return render(request, 'inputsheet.html', {'data': data})

@login_required
@require_http_methods(["GET"])
def adminapproval(request):
    # Acceptance.objects.all().delete()
    if get_role(request.user) != "adm":
        return redirect("landing")
    else:
        # ppx_need_approval = AdminApproval.objects.all()
        ppx_need_approval = Prioritization.objects.all()
        count = len(ppx_need_approval)
        if count == 0:
            print("no records")
            return redirect('landing')
        if count == 1:
            print("accept the one record")
            return redirect('landing')
        if count >1:
            population = list(range(1,count+1))
            k = count/2
            import random
            rand_list = random.sample(population, int(k))
            adminapproval_data = []
            for counter, value in enumerate(ppx_need_approval, 1):
                print(counter, value)
                approved = "X"
                if counter not in rand_list:
                    AdminApproval.objects.create(
                        entrytimestamp=value.entrytimestamp,
                    part_family = value.part_family,
                    grninputpart = value.grninputpart,
                    aging = value.aging,
                    priority = value.priority,
                    remarks = value.remarks
                    )
                    approved = "APPROVED"
                adminapproval_data.append([approved, value.entrytimestamp,value.part_family,value.grninputpart,
                                           value.aging,value.priority,value.remarks])

            context = {"table_title": "Prioritization Approval Report",
                       'table_head': ["ADMIN APPROVAL","Part Family", "GRN Input Part", "Aging", "Priority Type", "Remarks"],
                       'table_data': adminapproval_data}

            email_send("PPX Admin Approval Report", 'emailformat.html', context)
            print(AdminApproval.objects.all())
            Prioritization.objects.all().delete()
            return redirect('landing')
        


@login_required()
@require_http_methods(["POST"])
@csrf_exempt
def post_inputsheet(request):
    if get_role(request.user) != "adm":
        return redirect("landing")
    if request.method == 'POST':
        inputsheet_data = [["",0]]
        if request.content_type == 'application/json':
            if request.body:
                inputsheet_data = json.loads(request.body)
        primary_check = []
        for each in inputsheet_data:
            primary_check.append(each[0])
        if len(set(primary_check)) == len(primary_check):
            rollbackdata = Part.objects.all()
            rollbackdata.delete()
            for each in inputsheet_data:
                try:
                    if each[1] == "0":
                        rollbackdata.delete()
                        for each_rollback in rollbackdata:
                            Part.objects.create(part_number=each_rollback.part_number,
                                                inspection_lead_time=each_rollback.inspection_lead_time)
                        return JsonResponse({'data': "Inspection Lead time: 0, not allowed"})
                    Part.objects.create(part_number=each[0], inspection_lead_time=each[1])
                except Exception as ex:
                    print(each[0], ex)
                    for each_rollback in rollbackdata:
                            Part.objects.create(part_number=each_rollback.part_number, inspection_lead_time=each_rollback.inspection_lead_time)
                    return JsonResponse({'data': "Internal Error While saving"})
            print("Information saved to DB")
            return JsonResponse({'data': "Information saved to DB"})
        else:
            return JsonResponse({'data': "Part No. duplicates not allowed"})


@login_required()
@require_http_methods(["GET", "POST"])
def logoutuser(request):
    logout(request)
    return redirect('loginuser')


@login_required()
@require_http_methods(["GET", "POST"])
def landing(request):
    role = get_role(request.user)
    return render(request, "landing.html", {'role': role})
    # return render(request, "landing.html")

@login_required()
@require_http_methods(["GET", "POST"])
def emailformat(request):
    if get_role(request.user) != "adm":
        return redirect("landing")
    else:
        context = {"table_title": "PPX",
                   'table_head': ["1", "2", "3"],
                   'table_data': [["asdf", "asdf", "asdf"]]}
        return render(request, "emailformat.html", context)


Mdata = Mdata.objects.all()
@login_required()
@require_http_methods(["GET"])
def prioritization(request):
    print(get_role(request.user))
    if get_role(request.user) != "ppx":
        return redirect("landing")
    pf_data = []
    agingDict = {}
    for each in Mdata:
        pf_data.append(each.part_family)
        diffdate = datetime.now(timezone.utc) - each.grninputdate

        # timestamp = each.grninputdate.strftime("%Y-%m-%d %H:%M")
        agingDict[each.grninputpart] = str(diffdate.days)+" Days, "+str(math.ceil(diffdate.seconds/3600))+ " Hours"
    pf_dataset = set(pf_data)

    # data = Part.objects.all()
    # Mdata = Mdata.objects.all()
    return render(request, 'prioritization.html', {'Pdata': pf_dataset, 'Mdata': Mdata, 'Adata': agingDict})


@login_required()
@require_http_methods(["POST"])
@csrf_exempt
def post_prioritization(request):
    if get_role(request.user) != "ppx":
        return redirect("landing")
    if request.method == 'POST':

        entrytimestamp = timezone.now()
        prioritization_data = [[entrytimestamp,'', '', '', '', '']]
        if request.content_type == 'application/json':
            if request.body:
                prioritization_data = json.loads(request.body)
                # print("*******", prioritization_data)
        rollbackdata = Prioritization.objects.all()
        # rollbackdata.delete()
        print(prioritization_data)
        for each in prioritization_data:




            try:
                Prioritization.objects.create(entrytimestamp=entrytimestamp,
                                              part_family=each[0],
                                              grninputpart=each[1],
                                              aging=each[2],
                                              priority=each[3],
                                              remarks=each[4])
            except Exception as ex:
                print(each[0], ex)
                rollbackdata.delete()
                for each_rollback in rollbackdata:
                        Prioritization.objects.create(
                            entrytimestamp=each_rollback.entrytimestamp,
                            part_family=each_rollback.part_family,
                            grninputpart=each_rollback.grninputpart,
                            aging=each_rollback.aging,
                            priority=each_rollback.priority,
                            remarks=each_rollback.remarks)
                return JsonResponse({'data': "Internal Error While saving"})
        try:
            # context = {
            #     "title": "ppx email",
            #     "table_data": prioritization_data
            # }

            context = {"table_title": "PPX Prioritization Report",
                       'table_head': ["Part Family", "GRN Input Part", "Aging", "Priority Type", "Remarks"],
                       'table_data': prioritization_data}
            print(prioritization_data)

            email_send("PPX Prioritization Report", 'emailformat.html', context)
            # from django.core.mail import EmailMessage
            # from django.template.loader import render_to_string
            # email = EmailMessage('Hello',"test prioritization data here\n"+email_text, to=['palamanibhaskar@gmail.com'])
            # email.send()
            # print("sent")
        except Exception as ex:
            print(ex)
        return JsonResponse({'data': "Table information saved to DB"})



@login_required()
@require_http_methods(["GET"])
def acceptance(request):
    if get_role(request.user) != "pqa":
        return redirect("landing")
    acptinputdata = AdminApproval.objects.all()
    return render(request, 'acceptance.html', {'acptinputdata': acptinputdata})


@login_required()
@require_http_methods(["POST"])
@csrf_exempt
def post_acceptance(request):
    format_str = '%m/%d/%Y'
    if get_role(request.user) != "pqa":
        return redirect("landing")
    if request.method == 'POST':
        entrytimestamp = timezone.now()
        acceptance_data = [[entrytimestamp,'', '', '', '', '','','']]
        if request.content_type == 'application/json':
            if request.body:
                acceptance_data = json.loads(request.body)
                print("*******",acceptance_data)
        rollbackdata = Acceptance.objects.all()
        rollbackdata.delete()
        for each in acceptance_data:
            try:
                if each[5] == '':
                    each[5] = datetime.strptime('10/10/2010', format_str)
                else:
                    each[5] = datetime.strptime(each[5], format_str)
                if each[6] == '':
                    each[6] = datetime.strptime('10/10/2010', format_str)
                else:
                    each[6] = datetime.strptime(each[6], format_str)
                print(each[5], type(each[6]))

                Acceptance.objects.create(entrytimestamp=entrytimestamp,
                                              part_family=each[0],
                                              grninputpart=each[1],
                                              aging=each[2],
                                              priority=each[3],
                                              remarks=each[4],
                                          planned_completion=entrytimestamp,
                                          actual_completion=entrytimestamp)
            except Exception as ex:
                print(each[0], ex)
                for each_rollback in rollbackdata:
                        Acceptance.objects.create(
                            entrytimestamp=each_rollback.entrytimestamp,
                            part_family=each_rollback.part_family,
                            grninputpart=each_rollback.grninputpart,
                            aging=each_rollback.aging,
                            priority=each_rollback.priority,
                            remarks=each_rollback.remarks,
                            planned_completion=each_rollback.entrytimestamp,
                            actual_completion=each.rollback.entrytimestamp)
                return JsonResponse({'data': "Internal Error While saving"})
        try:
            context = {"table_title": "PQA Acceptance Report",
                       'table_head': ["Part Family", "GRN Input Part", "Aging", "Priority Type", "Remarks","Planned DT", "Actual DT"],
                       'table_data': acceptance_data,
                       'id':str(entrytimestamp)}
            print(acceptance_data)

            email_send(str(entrytimestamp),'emailformat.html', context)
            print("acceptance mail sent")
        except Exception as ex:
            print(ex)
        return JsonResponse({'data': "Table information saved to DB"})


@login_required()
@require_http_methods(["GET"])
def reports(request):
    if get_role(request.user) != "pqa":
        return redirect("landing")
    import xlrd
    import csv
    from django.conf import settings
    import os
    from django.views.static import serve

    xlsx_path = os.path.join(settings.BASE_DIR, 'master_data.xlsx')

    wb = xlrd.open_workbook(xlsx_path)
    sh = wb.sheet_by_name('SAPDATA')
    your_csv_file = open(os.path.join(settings.BASE_DIR, 'None\\media\\report_download.csv'), 'w')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))

    your_csv_file.close()

    return serve(request, os.path.join(settings.MEDIA_URL, 'report_download.csv'))


@login_required()
@require_http_methods(["GET", "POST"])
def dashboard(request):
    role = get_role(request.user)
    return render(request, "dashboard.html", {'role': role})


@login_required()
@require_http_methods(["GET", "POST"])
def dashboard_edit(request):
    role = get_role(request.user)
    return render(request, "dashboard.html", {'role': role})

@login_required()
@require_http_methods(["GET"])
def nok(request):
    from django_mailbox.models import Message
    from django.db.models import Q

    # python manage.py getmail
    data= {}
    print("No of Messages:", len(Message.objects.filter(Q(subject__icontains="NOK"))))
    for each in Message.objects.filter(Q(subject__icontains="NOK")):
        # data[each] = each.html()
        print(each.subject, each.html)
        return render(request, "nok.html", {'data': data})