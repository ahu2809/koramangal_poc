
# python manage.py shell

from eHeijunka.models import Part

Part.objects.all().delete()

pn_data = [["F00N.200.762", 86],
           ["F00N.201.319", 18],
           ["F002.FR0.005", 86],
           ["F002.F71.071", 10],
           ["F002.F71.072", 86],
           ["F002.FR1.023", 21],
           ["F00R.F00.018", 86],
           ["F00N.300.800", 10],
           ["F00N.300.700", 14],
           ["F002.FR0.100", 21],
           ["F002.F71.200", 14],
           ["F002.F72.300", 86],
           ["F002.FR1.400", 21],
           ["F00R.F00.500", 25],
           ["F00N.226.350", 86],
           ["F00N.301.450", 14],
           ["F002.FR1.550", 25],
           ["F002.F71.650", 21],
           ["F002.F71.750", 86],
           ["F002.FR1.850", 18],
           ["F00R.F00.950", 10]]


for each in pn_data:
    Part.objects.create(part_number=each[0], inspection_lead_time=each[1])

print(len(Part.objects.all()))


